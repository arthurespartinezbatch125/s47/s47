import React from 'react';
import {Container} from 'react-bootstrap';

import Course from './../components/Course';

import courses from './../mock-data/courses';

export default function Courses(){
	let CourseCards = courses.map((course) => {
		return <Course key={course.id} course={course}/>
	})
	return (
		<Container className="p-4">
			{CourseCards}
		</Container>
	)
}