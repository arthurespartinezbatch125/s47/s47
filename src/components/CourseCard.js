import React from 'react'
import {Row, Col, Card, Button} from 'react-bootstrap'

export default function CourseCard(){
	return(

		<Row className="justify-content-center my-5">
			<Col xs={10} md={6}>
				<Card>
				  <Card.Body>
				  	<Card.Title>Sample Course</Card.Title>
				  	<h5>Description</h5>
				  	<p>This is a sample course offering</p>
				  	<h5>Price:</h5>
				  	<p>Php 40000</p>
				    <Button variant="primary">Enroll</Button>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}